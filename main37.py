# -*- coding: utf-8 -*-
"""
Created on Tue Sep 19 15:56:02 2023

@author: 1
"""

import openpyxl as xl
from docx.shared import Cm
from docxtpl import DocxTemplate, InlineImage
from docx import Document

#Таблица 3.28
workbook1 = xl.load_workbook('Таблица 3.28 РФА 6 обр.xlsx')
sheet_1 = workbook1['Таблица 3.28']
table_contents = []

template = DocxTemplate('template37.docx')


for i in range(8, sheet_1.max_row + 1):
    
    table_contents.append({
        'a': "" if sheet_1.cell(i, 228).value==None else sheet_1.cell(i, 1).value,
        'b': "" if sheet_1.cell(i, 2).value==None else sheet_1.cell(i, 2).value,
        'c': "" if sheet_1.cell(i, 3).value==None else sheet_1.cell(i, 3).value,
        'd': "" if sheet_1.cell(i, 4).value==None else sheet_1.cell(i, 4).value,
        'e': "" if sheet_1.cell(i, 5).value==None else sheet_1.cell(i, 5).value,
        'f': "" if sheet_1.cell(i, 6).value==None else sheet_1.cell(i, 6).value,
        'g': "" if sheet_1.cell(i, 7).value==None else sheet_1.cell(i, 7).value,
        'h': "" if sheet_1.cell(i, 8).value==None else sheet_1.cell(i, 8).value,
        'i': "" if sheet_1.cell(i, 9).value==None else sheet_1.cell(i, 9).value,
        'j': "" if sheet_1.cell(i, 10).value==None else sheet_1.cell(i, 10).value,
        'k': "" if sheet_1.cell(i, 11).value==None else sheet_1.cell(i, 11).value,
        'l': "" if sheet_1.cell(i, 12).value==None else sheet_1.cell(i, 12).value,
        'm': "" if sheet_1.cell(i, 13).value==None else sheet_1.cell(i, 13).value,
        'n': "" if sheet_1.cell(i, 14).value==None else sheet_1.cell(i, 14).value,
        'o': "" if sheet_1.cell(i, 15).value==None else sheet_1.cell(i, 15).value,
        'p': "" if sheet_1.cell(i, 16).value==None else sheet_1.cell(i, 16).value,
        'q': "" if sheet_1.cell(i, 17).value==None else sheet_1.cell(i, 17).value,
        'r': "" if sheet_1.cell(i, 18).value==None else sheet_1.cell(i, 18).value,
        's': "" if sheet_1.cell(i, 19).value==None else sheet_1.cell(i, 19).value,
        't': "" if sheet_1.cell(i, 20).value==None else sheet_1.cell(i, 20).value,
        'u': "" if sheet_1.cell(i, 21).value==None else sheet_1.cell(i, 21).value,
        'v': "" if sheet_1.cell(i, 22).value==None else sheet_1.cell(i, 22).value,
        'w': "" if sheet_1.cell(i, 23).value==None else sheet_1.cell(i, 23).value,
        'x': "" if sheet_1.cell(i, 24).value==None else sheet_1.cell(i, 24).value,
        'y': "" if sheet_1.cell(i, 25).value==None else sheet_1.cell(i, 25).value,
        'z': "" if sheet_1.cell(i, 26).value==None else sheet_1.cell(i, 26).value,
        'aa': "" if sheet_1.cell(i, 27).value==None else sheet_1.cell(i, 27).value,
        'ab': "" if sheet_1.cell(i, 28).value==None else sheet_1.cell(i, 28).value,
        'ac': "" if sheet_1.cell(i, 29).value==None else sheet_1.cell(i, 29).value,
        'ad': "" if sheet_1.cell(i, 30).value==None else sheet_1.cell(i, 30).value,
        'ae': "" if sheet_1.cell(i, 31).value==None else round(sheet_1.cell(i, 31).value)
        })

context = {
    'table_contents': table_contents, 
    }
template.render(context)
template.save('Параграф37.docx')

print("Feature2_22")